$(document).ready(function() {

    $('#menu').click(function() {
        $(this).toggleClass('fa-times');
        $('header').toggleClass('toggle');
    });

    $(window).on('scroll load', function() {

        $('#menu').removeClass('fa-times');
        $('header').removeClass('toggle');
    });
    $(window).scroll(function() {
        if ($(this).scrollTop() >= 700) {
            $('#to-top').fadeIn();
        } else {
            $('#to-top').fadeOut();
        }
    });

    $("#to-top").click(function() {
        $("html, body").animate({ scrollTop: 0 }, 1000);
    });
});

// Typing Animation
var typed = new Typed('.typing', {
    strings: [
        "",
        "Front-End Dev",
        "Back-End Dev",
        "Web Developer",
        "Software Dev",
    ],
    typeSpeed: 100,
    backSpeed: 100,
    loop: true,
    startDelay: 2000,
    backDelay: 2000,
})